#!/bin/bash
LOCATION=$(pwd)

# Set this to your Atlassian instance's timezone.
# See this for a list of possible values:
# https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
TIMEZONE=$(cat /etc/timezone)

# include some configuration from a configuration file
source './credentials.jira.secret'

# typical contents:
#
# USERNAME=youruser
# PASSWORD=yourpassword
# INSTANCE=example.atlassian.net
# LOCATION=/where/to/store/the/file

# optionally, change location
BACKUP_URL="https://${INSTANCE}/rest/backup/1/export/runbackup"
PROGRESS_URL="https://${INSTANCE}/rest/backup/1/export/getProgress"

# Grabs cookies and generates the backup on the UI. 
TODAY=$(TZ=$TIMEZONE date +%Y%m%d)
COOKIE_FILE_LOCATION=jiracookie
curl --silent --cookie-jar $COOKIE_FILE_LOCATION -X POST "https://${INSTANCE}/rest/auth/1/session" -d "{\"username\": \"$USERNAME\", \"password\": \"$PASSWORD\"}" -H 'Content-Type: application/json' > /dev/null

#The $BKPMSG variable will print the error message, you can use it if you're planning on sending an email
BKPMSG=$(curl -s --cookie $COOKIE_FILE_LOCATION -H "Accept: application/json, text/javascript" -H "X-Requested-With: XMLHttpRequest" -H "Content-Type: application/json"  -X POST ${BACKUP_URL} -d '{"cbAttachments":"true", "exportToCloud": "true" }' )

# expected result:
#BKPMSG='{"taskId":"12345"}'
#echo $BKPMSG

TASKID=$(echo $BKPMSG | grep -Po '[0-9]+');
#echo $TASKID

#Checks if the backup procedure has failed
if [ "$(echo "$BKPMSG" | grep -ic backup)" -ne 0 ]; then
        echo "The request to start backup did not return expected message. Has export has run already recently?"

        echo "$BKPMSG"
        rm $COOKIE_FILE_LOCATION
        exit 1
fi

#Checks if the backup exists every 10 seconds, 20 times. If you have a bigger instance with a larger backup file you'll probably want to increase that.
for (( c=1; c<=20; c++ ))
        do

        PROGRESS_JSON=$(curl -s --cookie $COOKIE_FILE_LOCATION ${PROGRESS_URL}?taskId=$TASKID)
        PROGRESS=$(echo $PROGRESS_JSON | jq -r .progress)

        if [[ $PROGRESS_JSON == *"error"* ]]; then
                echo "error detected"
                break
        fi

        if [[ $PROGRESS == "100" ]] ; then
                FILE_NAME=$(echo "$PROGRESS_JSON" | jq -r .result)
                if [ ! -z "$FILE_NAME" ]; then
                        echo "using ${FILE_NAME} to download"
                        break
                fi
        fi
        sleep 20
done

#If it's confirmed that the backup exists the file get's copied to the $LOCATION directory.
if [[ $FILE_NAME == *"export/download"* ]]; then
        #remove httpOnly flag for cookies: http://www.perlmonks.org/?node_id=1187917
        sed -i 's/#HttpOnly_//g' jiracookie
        wget --load-cookies=$COOKIE_FILE_LOCATION -t 0 --retry-connrefused "https://${INSTANCE}/plugins/servlet/$FILE_NAME" -O "$LOCATION/JIRA-backup-${TODAY}.zip" >/dev/null 2>/dev/null
        rm $COOKIE_FILE_LOCATION
        echo "Download complete, backup file ${LOCATION}/JIRA-backup-${TODAY}.zip"
        exit 0
else
        echo "Attempted to download from WEBDAV directory, which is no longer supported"
        exit 1
fi
